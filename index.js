function countLetter(letter, sentence) {
    let result = 0;
    // const letter = "t"
    // const sentence = "The quick brown fox jumps over the lazy dog."
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    if (letter.length > 1) {
        // return `"${letter}" will render undefined result.`
        return undefined
    }
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i) == letter) {
            result += 1;
        }
    }
return result
}
let letter = 'e'
let sentence = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

console.log(countLetter(letter, sentence))

// ============================================
function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    return new Set(text.toLowerCase()).size === text.length;
}

let text = 'Racecar'
console.log(isIsogram(text))

// =============================
function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if (age < 13) {
        return undefined
    }
    else if (age > 12 && age < 22 || age > 64) {
         return (price*(1-discount)).toFixed(2)
    }
    else /*if (age > 21 && age < 65) */{
        return price.toFixed(2) 
    }
}
let age = 64
let discount = 0.20
let price = 100
console.log(purchase(age, price))

// =======================

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let zeroStocksCategory = [];
    let isExisting;

    for(let i = 0; i < items.length; i++){
        if(items[i].stocks === 0){
            if (!zeroStocksCategory.some(item =>item === items[i].category )) {
                zeroStocksCategory.push(items[i].category)
            }
        }
    }

    return zeroStocksCategory;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let flyingVoters = [];
 
    for(let a = 0; a < candidateA.length; a++){
        for(let b = 0; b < candidateB.length; b++){
            if (candidateA[a] === candidateB[b]) {
                if (!flyingVoters.some(voter => voter === candidateA[a])) {
                    flyingVoters.push(candidateA[a]);    
                }
            }
        }
    }

    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};